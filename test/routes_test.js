const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);


describe('forex_api_test_suite', () => {
	
	// it('test_api_get_rates_is_running', (done) => {
	// 	chai.request('http://localhost:5001').get('/forex/getRates')
	// 	.end((err, res) => {
	// 		assert.isDefined(res);
	// 		done();
	// 	})
	// })
	
	// it('test_api_get_rates_returns_200', (done) => {
	// 	chai.request('http://localhost:5001')
	// 	.get('/forex/rates')
	// 	.end((err, res) => {
	// 		assert.equal(res.status,200)
	// 		done();	
	// 	})		
	// })
	

	// it('test_api_post_currency_returns_200_if_complete_input_given', () => {
	// 	chai.request('http://localhost:5001')
	// 	.post('/forex/currency')
	// 	.type('json')
	// 	.send({
	// 		alias: 'nuyen',
	// 		name: 'Shadowrun Nuyen',
	// 		ex: {
	// 			'peso': 0.47,
	// 	        'usd': 0.0092,
	// 	        'won': 10.93,
	// 	        'yuan': 0.065
	// 		}
	// 	})
	// 	.end((err, res) => {
	// 		assert.equal(res.status,200)
			
	// 	})
	// })


	// Start CAPSTONE

	it("1. Test API Currency endpoint is running", (done) => {

		chai.request('http://localhost:5001')
		.get('/forex/currency')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})

	})


	it("2. Test Currency post if it returns 400 if no name property", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: "usd",
		      	// name: "United States Dollar",
		      	ex: {
		        	peso: 50.73,
		        	won: 1187.24,
		        	yen: 108.63,
		        	yuan: 7.03
		    	}
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})


	it("3. Test Currency post if it returns 400 if name is not a string", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: "usd",
		      	name: 100,
		      	ex: {
		        	peso: 50.73,
		        	won: 1187.24,
		        	yen: 108.63,
		        	yuan: 7.03
		    	}
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})

	it("4. Test Currency post if it returns 400 if name is an empty string", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: "usd",
		      	name: "",
		      	ex: {
		        	peso: 50.73,
		        	won: 1187.24,
		        	yen: 108.63,
		        	yuan: 7.03
		    	}
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})


	it("5. Test Currency post if it returns 400 if no ex property", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: "usd",
		      	name: "United States Dollar",
		     //  	ex: {
		     //    	peso: 50.73,
		     //    	won: 1187.24,
		     //    	yen: 108.63,
		     //    	yuan: 7.03
		    	// }
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})


	it("6. Test Currency post if it returns 400 if ex property is not an object", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: "usd",
		      	name: "United States Dollar",
		    	ex: "ex string"
		   
		    	// ex: {
		     //    	peso: 50.73,
		     //    	won: 1187.24,
		     //    	yen: 108.63,
		     //    	yuan: 7.03
		    	// }

			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})


	it("7. Test Currency post if it returns 400 if ex property is an empty object", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: "usd",
		      	name: "United States Dollar",
		      	ex: {}

		    	// ex: {
		     //    	peso: 50.73,
		     //    	won: 1187.24,
		     //    	yen: 108.63,
		     //    	yuan: 7.03
		    	// }

			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})	


	it("8. Test Currency post if it returns 400 if no alias property", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				// alias: "usd",
		      	name: "United States Dollar",
		      	ex: {
		        	peso: 50.73,
		        	won: 1187.24,
		        	yen: 108.63,
		        	yuan: 7.03
		    	}
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})


	it("9. Test Currency post if it returns 400 if alias is not a string", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: 200,
		      	name: "United States Dollar",
		      	ex: {
		        	peso: 50.73,
		        	won: 1187.24,
		        	yen: 108.63,
		        	yuan: 7.03
		    	}
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})


	it("10. Test Currency post if it returns 400 if alias is an empty string", (done) => {

		chai.request("http://localhost:5001")
			.post("/forex/currency")
			.type("json")
			.send({
				alias: "",
		      	name: "United States Dollar",
		      	ex: {
		        	peso: 50.73,
		        	won: 1187.24,
		        	yen: 108.63,
		        	yuan: 7.03
		    	}
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})

	})



	it("11. Test Currency post if it returns 400 if there are duplicate alias", (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: "usd",
			    name: "United States Dollar",
			    ex: {
			        peso: 50.73,
			        won: 1187.24,
			        yen: 108.63,
			        yuan: 7.03
			    }
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 400)
				done();
			})
	})


	it("12. Test Currency post if it returns 200", (done) => {
		chai.request('http://localhost:5001')
			.post('/forex/currency')
			.type('json')
			.send({
				alias: "cam",
			    name: "Camille New Currency",
			    ex: {
			        peso: 100,
			        won: 200,
			        yen: 300,
			        yuan: 400
			    }
			})
			.end((err, res) => {
				// console.log(res.status)
				assert.equal(res.status, 200)
				done();
			})
	})


})
