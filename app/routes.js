const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {

		// Start CAPSTONE

		let foundAlias = exchangeRates.find((exchangeRate) => {

			// console.log("alias: " + req.body.alias);
			return exchangeRate.alias === req.body.alias 

		});



		if(!req.body.hasOwnProperty("name")){
			return res.status(400).send({
				error: "Bad Request - missing required parameter NAME"
			})
		}

		if(!req.body.hasOwnProperty("ex")){
			return res.status(400).send({
				error: "Bad Request - missing required parameter EX"
			})
		}


		if(!req.body.hasOwnProperty("alias")){
			return res.status(400).send({
				error: "Bad Request - missing required parameter ALIAS"
			})
		}


		if(typeof req.body.name !== 'string'  ||  req.body.name === ""){
			// console.log(typeof req.body.name);
			// console.log("name: " + req.body.name);
			return res.status(400).send({
				error : 'Bad Request: NAME has to be a string'
			})
		}


		if(typeof req.body.ex !== 'object' ||  JSON.stringify(req.body.ex) === "{}"){
			// console.log(typeof req.body.ex);
			// console.log("ex: " + req.body.ex);
			return res.status(400).send({
				error : 'Bad Request: EX has to be an object'
			})
		}


		if(typeof req.body.alias !== 'string' ||  req.body.alias === ""){
			// console.log(typeof req.body.alias);
			// console.log("alias: " + req.body.alias);
			return res.status(400).send({
				error : 'Bad Request: ALIAS has to be a string'
			})
		}


		if(foundAlias){

			return res.status(400).send({
				'forbidden': 'Duplicate ALIAS found.'
			})
		}



		return res.status(200).send(exchangeRates);
	})


module.exports = router;
